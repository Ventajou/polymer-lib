This is a playground to test the usability of Polymer to create a framework agnostic UI library using Polymer.

# Global dependencies needed

    npm i -g gulp

# Build the library

    cd polymer
    npm i
    gulp

The gulp task will build the library and watch for file changes for rebuild. It will also copy the bundle to the ng1 and ng4 test apps.

# Build and run the ng1 test app

    cd ng1
    npm i
    gulp

This will build and serve the test app on http://localhost:3000

Note: I've not bothered to have the ng1 app watch for changes to the library so you need to manually reload for those to be picked up.

# Build and run the ng4 test app

    cd ng4
    npm i
    npm start

This will build and serve the test app on http://localhost:4200
