var build = require('@rightscale/ui-build-tools');
var gulp = require('gulp');

build.init({
  bundles: [{
    name: 'polymer-test',
    root: 'src',
    dist: true,
    run: {
      port: 3000
    }
  }]
});

gulp.task('default', ['polymer-test:run']);
