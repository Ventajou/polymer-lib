import Module from '@rightscale/ui-angular-decorators';

let app = new Module('polymer-test', ['polymer-test.templates', 'polymer-test.images', 'ui.router']);
export default app;
