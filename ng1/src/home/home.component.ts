import clone from 'lodash/clone';
import app from '../app';

@app.inject('$element').component({
  templateUrl: 'polymer-test/home/home.html'
})
export class Home {
  static readonly componentName = 'rsHome';

  items = [
    { label: 'foo', value: '1' },
    { label: 'bar', value: '2' },
    { label: 'baz', value: '3' }
  ];

  el: any;

  constructor($element: ng.IRootElementService) {
    this.el = $element.find('rs-dropdown')[0];
    this.el.items = this.items;
    this.el.selectedItem = this.items[0];
    // tslint:disable-next-line:no-console
    this.el.addEventListener('change', (e: any) => console.log('change:', e.detail, this.el.selectedItem));
  }

  addItem(label: string, value: string) {
    this.items.push({label, value});
    this.el.items = this.items.slice();
  }
}
