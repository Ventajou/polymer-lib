import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  items = [
    { label: 'Default Colors', value: '' },
    { label: 'Reds', value: 'red' },
    { label: 'Greens', value: 'green' },
    { label: 'Crazy', value: 'crazy' }
  ];

  selectedItem = this.items[0];

  changed(e) {
    this.selectedItem = e.detail.value;
  }
}
