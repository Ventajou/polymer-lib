const gulp = require('gulp');
const gutil = require("gulp-util");
const webpack = require("webpack");
const config = require('./webpack.config');

gulp.task('build', cb => {
  webpack(config, function(err, stats) {
    if (err) throw new gutil.PluginError("webpack", err);
    gutil.log("[webpack]", stats.toString());
    cb();
  });
});

gulp.task('copy-ng1', ['build'], () => {
  gulp.src('dist/**/*')
    .pipe(gulp.dest('../ng1/dist/polymer-test/js'))
});

gulp.task('copy-ng4', ['build'], () => {
  gulp.src('dist/**/*')
    .pipe(gulp.dest('../ng4/src/assets/polymer'))
});

gulp.task('default', ['copy-ng1', 'copy-ng4'], () => {
  gulp.watch('src/**/*', ['copy-ng1', 'copy-ng4']);
});
